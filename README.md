# Interlude

My journey of learning WGPU and GFX programming. I have experience with WGPU from about 2 months ago, i've almost completed [this tutorial](https://sotrh.github.io/learn-wgpu/)  but i still have a lot to learn. So let this repo serve as assistance for others in learning this witchcraftery.

## Notes
I will be taking markdown notes in the notes directory found in /notes. I will take notes as i go along with tutorials documentation and other resources i may find. ( Note that i use logseq to take these notes so for the best experience use logseq but i will probably end up creating a notes-raw directory for the raw markdown files )